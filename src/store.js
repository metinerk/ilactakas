import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from './router'
import jwt from 'jsonwebtoken'

Vue.use(Vuex)

const apiEndpoint = "https://ilactakasbackend.herokuapp.com"
/*const apiEndpoint = "http://localhost:3000"*/

export default new Vuex.Store({
    state: {
        bids: [],
        pharmacies: [],
        user: {
            name: ""
        },
        isAuth: false,
        alert: {
            state: null,
            text: null,
            type: null
        },
        aldiklarim: [],
        karekodBekleyenler: [],
        gonderilmeyeHazirOlanlar:[],
        bakiyeler: null

    },
    getters: {
        getUser: state => {
            return state.user
        },
        getAlert: state => {
            return state.alert
        },
        getPharmacies: state => {
            return state.pharmacies
        },
        getSortedBakiyeler: state => {
            state.bakiyeler.pharmacies = state.bakiyeler.pharmacies.sort( ( p, a) => {
                return a.bakiye - p.bakiye
            })
            return state.bakiyeler.pharmacies
        }
    },
    mutations: {

        GETBIDS: (state) => {
            getBids(state)
        },
        GETPHARMACIES: (state) => {
            axios.get(apiEndpoint +'/user/getPharmacies')
                .then( pharmacies => {

                    state.pharmacies = pharmacies.data.pharmacies
                    state.pharmacies = state.pharmacies.filter( pharmacy => {
                        return pharmacy.name !== state.user.name
                    })
                })
        },
        CREATEBID: (state, data) => {

            let reqWithAutHeader = axios.create({
                headers: {'Authorization': localStorage.getItem('token')}
            });

            reqWithAutHeader.post( apiEndpoint+'/bids/createBid', data)
                .then(response => {

                    if(response.status === 201){
                        state.alert = {
                            state: true,
                            text: 'TALEP BAŞARIYLA OLUŞTURULDU',
                            type: 'success'
                        }
                        setTimeout(() => {
                            state.alert.state = false
                        })
                        router.push('/bids')
                    }

                })
                .catch( err => {
                    if(err.response.status === 401){
                        state.isAuth = false
                        router.push('/')
                    }
                })
        },
        SIGNUP: (state, data) => {

            axios.post(apiEndpoint+'/user/signup', data)
                .then(response => {
                    router.push('/')
                    state.alert = {
                        state: true,
                        text: 'KAYIT BAŞARILI',
                        type: 'success'
                    }
                    setTimeout(()=>{
                        state.alert.state = false
                    },5000)
                })
                .catch( err => {
                    state.alert = {
                        state: true,
                        text: 'E-MAİL KULLANILMAKTA',
                        type: 'error'
                    }
                    setTimeout(()=>{
                        state.alert.state = false
                    },3000)
                })
        },
        LOGIN: (state, data) => {
            axios.post(apiEndpoint+'/user/login', data)
                .then(response => {
                    localStorage.setItem("token", "Bearer " + response.data.token)
                    state.isAuth = true
                    state.user = jwt.decode(response.data.token).user
                    state.alert = {
                        state: true,
                        text: 'GİRİŞ BAŞARILI',
                        type: 'success'

                    }
                    setTimeout(()=>{
                        state.alert.state = false
                    },3000)
                    router.push('/bids')
                })
                .catch(err => {
                    state.alert = {
                        state: true,
                        text: 'LÜTFEN BİLGİLERİ KONTROL EDİNİZ.',
                        type: 'error'

                    }
                    setTimeout(() => {
                        state.alert.state = false
                    },2000)
                })
        },
        LOGOUT: (state) => {
            state.isAuth = false
            state.user ={
                name: ""
            }
            localStorage.clear()
            router.push('/')
            state.alert = {
                state: true,
                text: 'ÇIKIŞ YAPILDI',
                type: 'success'

            }
            setTimeout(() => {
                state.alert.state = false
            },2000)
        },

        //check if current session has valid token
        CHECKAUTH: ( state ) => {
            if( localStorage.getItem('token')){
                let tokenObject = {
                    token : localStorage.getItem('token')
                }
                axios.post(apiEndpoint + '/user/verifyToken' , tokenObject )
                    .then( response => {
                        if( response.data.isVerified){
                            let decoded = jwt.decode(localStorage.getItem('token').split(" ")[1])
                            state.user = decoded.user
                            state.isAuth = true
                            router.push('/bids')
                        }else{
                            state.isAuth = false
                            state.user = {
                                name:''
                            }
                        }
                    })
            }
        },

        INVOKEALERT: (state, data) => {
            state.alert = {
                state: true,
                text: data.text,
                type: data.type
            }
            setTimeout(() => {
                state.alert.state = false
            },4000)
        },
        DOALIM: (state , data) => {

            let reqWithAutHeader = axios.create({
                headers: {'Authorization': localStorage.getItem('token')}
            });
            reqWithAutHeader.post(apiEndpoint + '/bids/alimYap', data)
                .then( response => {
                    getBids(state)
                })
                .catch( err => {
                    if(err.response.status === 401){
                        state.isAuth = false
                        router.push('/')
                    }
                })
        },
        GETALDIKLARIM: (state, data) =>{
            let reqWithAutHeader = axios.create({
                headers: {'Authorization': localStorage.getItem('token')}
            })
            reqWithAutHeader.post(apiEndpoint + '/bids/getAldiklarim', data)
                .then( response => {
                    state.aldiklarim = response.data

                    state.aldiklarim.alinanTeklifler = state.aldiklarim.alinanTeklifler.sort((a,b) => {
                        return new Date(b.alimVeGonderimBilgisi.date) - new Date(a.alimVeGonderimBilgisi.date)
                    })

                })
                .catch( err => {
                    if(err.response.status === 401){
                        state.isAuth = false
                        router.push('/')
                    }
                })
        },
        GETGONDERIMLERIM: (state, data) => {
            let reqWithAutHeader = axios.create({
                headers: {'Authorization': localStorage.getItem('token')}
            })
            reqWithAutHeader.post(apiEndpoint + '/bids/getGonderimlerim', data)
                .then( response => {
                    let gonderilecekler = response.data.gonderilecekler
                    gonderilecekler = gonderilecekler.sort((a,b) => {
                        return new Date(b.alimVeGonderimBilgileri.date) - new Date(a.alimVeGonderimBilgileri.date)
                    })

                    //clean the arrays
                    state.karekodBekleyenler = []
                    state.gonderilmeyeHazirOlanlar = []

                    for(let i=0;i<gonderilecekler.length;i++){
                        if(gonderilecekler[i].alimVeGonderimBilgileri.karekodGirildimi && gonderilecekler[i].alimVeGonderimBilgileri.gonderildi){
                            state.gonderilmeyeHazirOlanlar.push(gonderilecekler[i])
                        }else{
                            state.karekodBekleyenler.push(gonderilecekler[i])
                        }
                    }
                })
                .catch( err => {
                    if(err.response.status === 401){
                        state.isAuth = false
                        router.push('/')
                    }
                })
        },
        GETBAKIYELER: ( state, data ) => {

            let reqWithAutHeader = axios.create({
                headers: {'Authorization': localStorage.getItem('token')}
            })

            reqWithAutHeader.post(apiEndpoint+ '/user/getBakiyeler', data)
                .then( response => {
                    state.bakiyeler = response.data
                })
        }

    },
    actions: {
        getBids: ({commit}) => {
            commit('GETBIDS')
        },
        getPharmaciesAction: ({ commit }) => {
            commit('GETPHARMACIES')
        },
        createBidAction: ({commit}, data) => {
            commit('CREATEBID', data)
        },
        signUp: ({commit}, data) => {
            commit("SIGNUP", data)
        },
        login: ({commit}, data) => {
            commit("LOGIN", data)
        },
        logout: ({commit}) => {
            commit('LOGOUT')
        },
        checkAuth: ({ commit }) => {
            commit('CHECKAUTH')
        },
        invokeAlert: ({ commit }, data) => {
            commit('INVOKEALERT', data)
        },
        doAlim: ({ commit }, data) => {
            commit('DOALIM', data)
        },
        getAldiklarim: ({ commit}, data ) => {
            commit('GETALDIKLARIM', data)
        },
        getGonderimlerim: ({commit}, data) => {
            commit('GETGONDERIMLERIM', data)
        },
        setKarekods: ({ commit,state }, data) => {

            return new Promise((resolve, reject) => {

                let reqWithAutHeader = axios.create({
                    headers: {'Authorization': localStorage.getItem('token')}
                })
                reqWithAutHeader.post(apiEndpoint + '/bids/setKarekods', data)
                    .then( response => {

                        let data = {
                            id: state.user._id
                        }
                        commit('GETGONDERIMLERIM', data)
                        resolve(response)

                    })
                    .catch( err => {
                        if(err.response.status === 401){
                            state.isAuth = false
                            router.push('/')
                        }
                        reject(err)
                    })

            })

        },
        gonder: ({ commit,state }, data) => {

            return new Promise((resolve, reject) => {

                let reqWithAutHeader = axios.create({
                    headers: {'Authorization': localStorage.getItem('token')}
                })
                reqWithAutHeader.post(apiEndpoint + '/bids/gonder', data)
                    .then( response => {

                        let data = {
                            id: state.user._id
                        }
                        commit('GETGONDERIMLERIM', data)
                        commit('GETBAKIYELER')
                        resolve(response)

                    })
                    .catch( err => {
                        if(err.response.status === 401){
                            state.isAuth = false
                            router.push('/')
                        }
                        reject(err)
                    })

            })

        },
        teslimAl: ({ state }, data) => {

            return new Promise((resolve, reject) => {

                let reqWithAutHeader = axios.create({
                    headers: {'Authorization': localStorage.getItem('token')}
                })
                reqWithAutHeader.post(apiEndpoint + '/bids/teslimAl', data)
                    .then( response => {
                        resolve(response)

                    })
                    .catch( err => {
                        if(err.response.status === 401){
                            state.isAuth = false
                            router.push('/')
                        }
                        reject(err)
                    })

            })

        },
        getBakiyeler: ( { commit }, data) => {

            commit('GETBAKIYELER', data)
        },
        setBakiye: ( { state, commit },data ) => {
            return new Promise( (resolve, reject) => {
                let reqWithAutHeader = axios.create({
                    headers: {'Authorization': localStorage.getItem('token')}
                })
                reqWithAutHeader.post(apiEndpoint + '/user/setBakiye', data)
                    .then( response => {
                        commit('GETBAKIYELER')
                        router.push('/bakiyeler')
                        resolve(response)
                    })
                    .catch( err => {
                        if(err.response.status === 401){
                            state.isAuth = false
                            router.push('/')
                        }
                        reject(err)
                    })
            })
        }
    },

})

function getBids(state){
    let reqWithAutHeader = axios.create({
        headers: {'Authorization': localStorage.getItem('token')}
    });
    reqWithAutHeader.get(apiEndpoint+'/bids/getBids')
        .then(response => {
            state.bids = response.data.bids

        })
        .catch( err => {
            if(err.response.status === 401){
                state.isAuth = false
                router.push('/')
            }
        })
}
