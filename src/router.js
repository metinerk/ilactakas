import Vue from 'vue'
import Router from 'vue-router'
import CreateBid from './views/CreateBid.vue'
import Bids from './views/Bids.vue'
import Login from './views/Login.vue'
import Signup from './views/Signup.vue'
import Aldiklarim from './views/Aldiklarim.vue'
import Gonderimlerim from './views/Gonderimlerim.vue'
import Bakiyeler from './views/Bakiyeler.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/createBid',
      name: 'createBid',
      component: CreateBid
    },
    {
      path: '/bids',
      name: 'bids',
      component: Bids
    },
    {
      path: '/signUp',
      name: 'signUp',
      component: Signup
    },
    {
        path: '/aldiklarim',
        name: 'aldiklarim',
        component: Aldiklarim
    },
    {
        path: '/gonderimlerim',
        name: 'gonderimlerim',
        component: Gonderimlerim
    },
    {
        path: '/bakiyeler',
        name: 'bakiyeler',
        component: Bakiyeler
    }
  ]
})
